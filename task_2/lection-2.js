/*
 * First Task
 *
 */

 function multiplier(x) {
   return function(y){
     return x*y;
   };
 }

 function processData(input) {
   const waterWeight   = multiplier(1000);
   const mercuryWeight = multiplier(1355);
   const oilWeight     = multiplier(900);

   console.log("Weight of " + input + " metric cube of water = " + waterWeight(input) + " kg");
   console.log("Weight of " + input + " metric cube of mercury = " + mercuryWeight(input) + " kg");
   console.log("Weight of " + input + " metric cube of oil = " + oilWeight(input) + " kg");
}

/*
 * Second Task
 *
 */

function makeRandomFn( param ) {
  return function() { 
    return param[ Math.floor(Math.random()*param.length) ]; 
  };
}

const getRandomNumber = makeRandomFn([1, 2, 100, 34, 45, 556, 33])
console.log(getRandomNumber());
console.log(getRandomNumber());
console.log(getRandomNumber());
  
/*
 * Third Task
 *
 */
function makeRandomFn( param ) {    
  param = ( typeof param === 'number' ) ? Array.from(arguments) : param;
  return function() { 
    return param[ Math.floor(Math.random()*param.length) ]; 
  }();
}

console.log( makeRandomFn(1, 2, 100, 34, 45, 556, 33) );
console.log( makeRandomFn([1, 2, 100, 34, 45, 556, 33]) );
